*** Settings ***
Library         DebugLibrary
Resource          ../../resource/keywords/browser_keywords.robot

Suite Teardown    Close Browser

*** Test Cases ***
limehome test task
    [Documentation]    e2e test for validatind guest lastname
    ...                on first screen and refistrationForm
    [Tags]
    Open Browser to Page
    Wait Until Page Contains Element    ${1st screen lastName input field}
    Input Text    ${1st screen lastName input field}    ${lastName}
    Wait Until Page Contains Element    ${1st screen bookingReference input field}
    Input Text    ${1st screen bookingReference input field}    ${bookingRef}
    Click Element     ${submit button}
    Capture Page Screenshot
    Wait Until Page Contains Element    ${2nd screen lastName input field}
    Wait Until Page Contains Element    ${2nd screen bookingReference}
    Capture Page Screenshot
# User way of picking date of Birth
    Click Element    ${open calendar button}
    Click Element    ${choose date button}
    Click Element    ${choose mounth and year button}
    Click Element    ${choose previous 20 years button}
    Click Element    ${1992}
    Click Element    ${August 1992}
    Click Element    ${August 11, 1992}
    Capture Page Screenshot
    Click Element    ${2nd screen nationality field}
    Click Element    ${nationality Danish}
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Wait Until Page Contains Element    ${upload button}
    Capture Page Screenshot
#    Debug
