*** Settings ***

Documentation   Importing web and robotframework built-in keywords.
Library  SeleniumLibrary   timeout=10
Library    Collections
Library    OperatingSystem
Variables         ../../resource/settings/const.py
Resource        ../../resource/settings/page_objects.robot
*** Variables ***
${BROWSER}        chrome
${DELAY}          0.3
${SELENOID_SERVER}    http://localhost:4444/wd/hub

*** Keywords ***
Open Browser to Page
    [Documentation]
    ${desired_caps} =   create dictionary  enableVNC=${True}
    Open Browser    ${PAGE_URL}    ${BROWSER}    None    ${SELENOID_SERVER}    desired_capabilities=${desired_caps}

    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
