*** Variables ***
# 1st screen /
${1st screen lastName input field}           css=input[formcontrolname="lastName"]
${1st screen bookingReference input field}   css=input[formcontrolname="bookingReference"]

${submit button}                             xpath://*[contains(text(),'Submit')]

# 2nd screen /registrationForm
${2nd screen lastName input field}           xpath://*[contains(@ng-reflect-model, "${lastName}")]
${2nd screen bookingReference}               xpath://*[contains(text(), "${bookingRef}")]
${2nd screen dateOfBirth input field}        xpath://*[@id="mat-input-2"]
${2nd screen streetAdress input field}       xpath://*[@id="mat-input-3"]
${2nd screen postalCode input field}         xpath://*[@id="mat-input-4"]
${2nd screen city input field}               xpath://*[@id="mat-input-5"]
${2nd screen nationality field}              xpath://*[@id="mat-select-1"]
${nationality Danish}                        xpath://*[@id="mat-option-161"]

${open calendar button}                      xpath://*[contains(@aria-label, "Open calendar")]
${choose date button}                        xpath://*[contains(@aria-label, "Choose date")]
${choose mounth and year button}             xpath://*[contains(@aria-label, "Choose month and year")]
${choose previous 20 years button}           xpath://*[contains(@aria-label, "Previous 20 years")]
${1992}                                      xpath://*[contains(@aria-label, "1992")]
${August 1992}                               xpath://*[contains(@aria-label, "August 1992")]
${August 11, 1992}                           xpath://*[contains(@aria-label, "August 11, 1992")]

${upload button}                             xpath://*[contains(text(), "Upload")]
