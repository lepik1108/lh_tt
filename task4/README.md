Run test with:
 ```sh
 $ cd task4 && pip3 install -r requirenments.txt -U && robot tests
 ```

Requirements:
 - docker installed
 - your user is added to docker group
 - selenoid installed and running in docker mode
 
Setup selenoid manually on hosts:

-on linux/macos
 - using aerokube/cm with docker
 ```sh
 $ curl -s https://aerokube.com/cm/bash | bash && rm ~/.aerokube/selenoid/browsers.json && ./cm selenoid configure --vnc --last-versions 2 --config-dir ./ && ./cm selenoid start --vnc --config-dir ./ && ./cm selenoid-ui update
 ```
-on Windows
 - using aerokube/cm with docker
 ```sh
 $ curl -s https://aerokube.com/cm/bash | bash && rm ~/.aerokube/selenoid/browsers.json && ./cm selenoid configure --vnc --last-versions 2 --config-dir ./ && ./cm selenoid start --vnc --config-dir ./ && ./cm selenoid-ui update
 ```

Notes:
 - You're able to see ongoing test execution on selenoid web interface(http://localhost:8080)