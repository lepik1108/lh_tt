Limehome AQA test task.

## Answers

#### Task #1: Writing Test Scenarios

Positive scenarios, based on core feature description:

#### Scenario #1:
```
 Feature: Retrive the reservation details using the reference number and guest last name
    1 Given I am a guest
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    4 When I click Submit button
    5 Then I see second screen(/registrationForm)
    6 And my LastName is Preston
    7 And my bookingReference is 0A1B2C
    8 Then I am filling in all input fields 
    9 When I press Submit button
    10 Then I see third screen(/complete)
    11 Then I resresh page
    12 And I am on first screen(/)
    13 When I repeat steps [1-4]
    15 Then I see screen(/registrationForm) with form, filled in as it was after step 8
```

#### Scenario #2:
```
 Feature: Capability to take/upload a picture of the passport that's a mandatory field for non-german nationalities
    1 Given I am a guest
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    4 When I click Submit button
    5 Then I see second screen(/registrationForm)
    6 And my LastName is Preston
    7 And my bookingReference is 0A1B2C
    8 Then I am filling in all input fields
    9 When changing Nationality field from DE to any other
    9 Then I see PassportID input field
    10 And I see passport picture upload button
    11 When I click passport picture upload button
    12 Then I see my file manager dialog
    13 When I select some picture
    14 Then I see that picture displayed on registrationForm
    15 When I press Submit button
    16 Then I see third screen(/complete)
```

#### Scenario #3:
```
 Feature: Mobile friendly web app
    1 Given I am lazy AQA
    2 And I make screenshot of all pages with screen resolution 600x600
    3 And I make screenshot of all pages with screen resolution 640x640
    4 Then I scale them both to 800x800
    5 When I compare screenshots of same page with opencv library
    6 Then I see difference in styles
    7 And can visualize them with color frames
```
#### Scenario #3a:
```
 Feature: Mobile friendly web app
    1 Given I am a guest with screen width <= 575
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 And input fields presented in 2 separated lines
    4 And "Retrieve your booking" block displaying above "Online check-in" block
```

#### Scenario #3b:
```
 Feature: Mobile friendly web app
    1 Given I am a guest with screen width > 575 and <= 600
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 And input fields presented in 2 separated lines
    4 And "Retrieve your booking" block displaying under "Online check-in" block
    5 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    6 When I click Submit button
    7 Then I see second screen(/registrationForm)
    8 And all input fields except Type of stay:* presented in 2 rows
    9 When changing Nationality field from DE to any other
    10 Then I see input fields presented PassportID and passport picture in 2 separated lines
```

#### Scenario #3c:
```
 Feature: Mobile friendly web app
    1 Given I am a guest with screen width > 600
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 And input fields presented in 1 line
    4 And "Retrieve your booking" block displaying under "Online check-in" block
    5 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    6 When I click Submit button
    7 Then I see second screen(/registrationForm)
    8 And all input fields except Type of stay:* presented in 3 rows
    9 When changing Nationality field from DE to any other
    10 Then I see input fields presented PassportID and passport picture in 2 separated lines
```

#### Scenario #4:
```
 Feature: Needs to be bilingual (german / english)
    1 Given I am lazy AQA
    2 And I do GET https://limehome-qa-task.herokuapp.com/assets/i18n/en.json
    3 And I do GET https://limehome-qa-task.herokuapp.com/assets/i18n/de.json
    4 Then I iterate through json response text field's keys
    5 And validate they're not identical
```

#### Scenario #4a:
```
 Feature: Needs to be bilingual (german / english)
    1 Given I am a guest
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 And I see language radio button in top right corner
    4 And default language is Engineer
    5 When I click on DE
    6 Then I see page text switched to German
```

#### Scenario #4b:
```
 Feature: Needs to be bilingual (german / english)
    1 Given I am a guest
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    4 When I click Submit button
    5 Then I see second screen(/registrationForm)
    6 And I see language radio button in top right corner
    7 And default language is Engineer
    8 When I click on DE
    9 Then I see page text switched to German
    10 When changing Nationality field from DE to any other
    11 Then I see input fields presented PassportID and passport picture 
    12 And PassportID and passport picture fields descriptions have been translated to German
```

#### Scenario #4c:
```
 Feature: Needs to be bilingual (german / english)
    1 Given I am a guest
    2 Then I am on the first page(/) (https://limehome-qa-task.herokuapp.com/)
    3 Then I am filling in the input fields: LastName - Preston, bookingReference - 0A1B2C
    4 When I click Submit button
    5 Then I see second screen(/registrationForm)
    6 And my LastName is Preston
    7 And my bookingReference is 0A1B2C
    8 Then I am filling in all input fields 
    9 When I press Submit button
    10 Then I see third screen(/complete)
    11 And I see language radio button in top right corner
    12 And default language is Engineer
    13 When I click on DE
    14 Then I see page text switched to German
```

#### Tasks #2 and #3: Finding Defects and Defect Reporting

I'll choose one defect and make an open issue, using gitlab. Btw all scenarios above, exept #2 and #3 got failed, and there're some reportable things in #3.

#### Task #4: Automation (Bonus)

Test will be written according to your recomendations, and stored [here](https://gitlab.com/lepik1108/lh_tt/-/tree/master/task4)


##### If you were a user, what would you like to change to have a better online check-in experience?
 - left a comment with answer [here](https://gitlab.com/lepik1108/lh_tt/-/issues/1#note_379808945)

